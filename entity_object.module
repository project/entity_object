<?php

/**
 * @file
 * Main module implementation
 */

/**
 * Implements hook_menu().
 */
function entity_object_menu() {

  $items['entity-object/file/upload'] = array(
    'type' => MENU_CALLBACK,
    'page callback' => 'entity_object_file_upload',
    'access callback' => 'entity_object_file_upload_access',
  );

  $items['entity-object/file/progress'] = array(
    'type' => MENU_CALLBACK,
    'page callback' => 'entity_object_file_progress',
    'access callback' => 'entity_object_file_upload_access',
  );

  return $items;
}

/**
 * Returns the list of entity_types that enables tree-mode export.
 *
 * @param bool $reset
 *   whether reset the cached static list or not
 *
 * @return array
 *   an associative array of associative arrays keyed by entity type whose sub
 *   keys are :
 *     - bundles: an array of bundle names that support tree-mode export. if
 *       omitted, all defined bundles of this entity type supports tree-mode
 *       export.
 * @see hook_entity_object_tree_entity_types()
 */
function entity_object_tree_entity_types($reset = FALSE) {
  $tree_entity_types = & drupal_static(__FUNCTION__, NULL, $reset);
  if (!isset($tree_entity_types)) {
    $tree_entity_types = array();
    $tree_entity_types = module_invoke_all('entity_object_tree_types');
  }
  return $tree_entity_types;
}

/**
 * Export an entity to its Object API representation.
 *
 * @param Entity $entity
 *   the entity to export to object API
 * @param EntityObjectContextInterface $context
 *   The Entity API handlers execution context.
 *
 * @return object|bool
 *   the entity exported to object API or FALSE in case of errors.
 */
function entity_object_export($entity, $context = NULL) {

  $object = new stdClass();

  $languages = array();

  $handler = entity_translation_get_handler($entity->wrapper()->type(), $entity);
  $translations = $handler->getTranslations();
  if (empty($translations->original)) {
    $handler->initTranslations();
  }
  if (!empty($translations->data)) {
    $languages = array_keys($translations->data);
  }

  $property_info = $entity->wrapper()->getPropertyInfo();

  $object->_meta = new stdClass();
  $object->_meta->translations = $languages;
  $object->_meta->entityType = $entity->wrapper()->type();

  foreach ($property_info as $property_name => $info) {
    $alias = entity_object_field_alias($property_name);
    if (!$alias) {
      $alias = $property_name;
    }
    if (!empty($info['translatable']) && $info['translatable'] && count($languages)) {
      foreach ($languages as $language) {
        $value = entity_object_export_value($entity->wrapper()->language($language)->{$property_name}, $context);
        if ($value !== NULL) {
          if (empty($object->{$alias})) {
            $object->{$alias} = new stdClass();
          }
          $object->{$alias}->{$language} = $value;
        }
      }
    }
    else {
      $value = entity_object_export_value($entity->wrapper()->{$property_name}, $context);
      if ($value !== NULL) {
        $object->{$alias} = $value;
      }
    }
  }

  return $object;

}

/**
 * Export a wrapper value as an object API value.
 *
 * This function will iterate over list wrapper and export values
 * individually.
 *
 * @param EntityMetadataWrapper $wrapper
 *   an entity wrapper
 * @param EntityObjectContextInterface $context
 *   TRUE if in tree mode, FALSE otherwise
 *
 * @return object|array|string
 *   the object API representation of this wrapper value
 */
function entity_object_export_value($wrapper, $context = NULL) {

  $wrapper_info = $wrapper->info();

  // If this is a field, we retrieve handler from field settings.
  if (!empty($wrapper_info['field'])) {
    $handler = entity_object_field_handler($wrapper_info['name']);
  }
  else {
    // This non-field property references another entity.
    if (entity_get_info($wrapper->type())) {
      $handler = entity_object_handler('entityreference');
    }
    else {
      $handler = entity_object_handler('entity_metadata_wrapper');
    }
  }

  if ($handler && function_exists($handler['entity_object_export'])) {
    $handler_function = $handler['entity_object_export'];
  }

  if (!empty($handler_function)) {
    if (!$handler['handle_list'] && entity_property_list_extract_type($wrapper->type())) {
      $result = array();
      foreach ($wrapper as $item) {
        $result[] = entity_object_handler_invoke('entity_object_export', $handler, $item, $context);
      }
    }
    else {
      $result = entity_object_handler_invoke('entity_object_export', $handler, $wrapper, $context);
    }
    return $result;
  }
  else {
    return NULL;
  }
}

/**
 * Import a wrapper value from an object API value.
 *
 * This function will iterate over list wrapper and import values
 * individually.
 *
 * @param EntityMetadataWrapper $wrapper
 *   an entity wrapper
 * @param array|object $value
 *   the value to import
 * @param EntityObjectContextInterface $context
 *   TRUE if in tree mode, FALSE otherwise
 *
 * @return bool
 *   TRUE is import succeed, FALSE otherwise
 */
function entity_object_import_value($wrapper, $value, $context = NULL) {

  $wrapper_info = $wrapper->info();

  // If this is a field, we retrieve handler from field settings.
  if (!empty($wrapper_info['field'])) {
    $handler = entity_object_field_handler($wrapper_info['name']);
  }
  else {
    // This non-field property references another entity.
    if (entity_get_info($wrapper->type())) {
      $handler = entity_object_handler('entityreference');
    }
    else {
      $handler = entity_object_handler('entity_metadata_wrapper');
    }
  }

  if ($handler && function_exists($handler['entity_object_import'])) {
    $handler_function = $handler['entity_object_import'];
  }

  if (!empty($handler_function)) {
    if (!$handler['handle_list'] && entity_property_list_extract_type($wrapper->type())) {
      $result = array();
      foreach ($value as $key => $item) {
        $result[] = entity_object_handler_invoke('entity_object_import', $handler, $wrapper[$key], $context, $item);
      }
    }
    else {
      $result = entity_object_handler_invoke('entity_object_import', $handler, $wrapper, $context, $value);
    }
    return $result;
  }
  else {
    return NULL;
  }
}


/**
 * Invoke one handler for one wrapper.
 *
 * @param string $handler_name
 *   the handler name (entity_object_import or entity_object_export).
 * @param array $handler
 *   the handler definition array.
 * @param EntityMetadataWrapper $wrapper
 *   the wrapper to export.
 * @param EntityObjectContextInterface $context
 *   the export context.
 * @param mixed $value
 *   the wrapper value.
 *
 * @return object
 *   the export result.
 */
function entity_object_handler_invoke($handler_name, $handler, $wrapper, $context, $value = NULL) {
  $handler_function = $handler[$handler_name];
  if (!empty($context)) {
    foreach ($context->handlerPreCallbacks() as $callback) {
      if (function_exists($callback)) {
        call_user_func_array($callback, array($wrapper, $handler, $context));
      }
    }
  }
  $handler_args = $handler_name == 'entity_object_import' ?
      array($wrapper, $value, $context) :
      array($wrapper, $context);

  $result = call_user_func_array($handler_function, $handler_args);
  if (!empty($context)) {
    foreach ($context->handlerPostCallbacks() as $callback) {
      if (function_exists($callback)) {
        call_user_func_array($callback,
          array($wrapper, $handler, $result, $context));
      }
    }
  }
  return $result;
}

/**
 * Import an object API representation to an entity object.
 *
 * @param object $object
 *   the object API representation to import into entity
 * @param Entity $entity
 *   the entity into which import data
 * @param EntityObjectContextInterface $context
 *   The Entity API handlers execution context.
 *
 * @return bool
 *   TRUE is case of success, FALSE otherwise.
 */
function entity_object_import($object, $entity, $context) {

  $languages = array();

  $handler = entity_translation_get_handler($entity->wrapper()->type(), $entity);
  $translations = $handler->getTranslations();
  if (empty($translations->original)) {
    $handler->initTranslations();
  }
  if (!empty($translations->data)) {
    $languages = array_keys($translations->data);
  }

  $property_info = $entity->wrapper()->getPropertyInfo();

  $context = NULL;

  // TODO handle import failures.
  foreach ($property_info as $property_name => $info) {
    $alias = entity_object_field_alias($property_name);
    if (!$alias) {
      $alias = $property_name;
    }
    if (!empty($info['translatable']) && $info['translatable'] && count($languages)) {
      $data = (object) $object->{$alias};
      foreach ($languages as $language) {
        if (isset($data->{$language})) {
          $value = entity_object_import_value($entity->wrapper()->language($language)->{$property_name}, $data->$language, $context);
        }
      }
    }
    else {
      if (isset($object->{$alias})) {
        $value = entity_object_import_value($entity->wrapper()->{$property_name}, $object->{$alias}, $context);
      }
    }
  }

  return TRUE;
}

/**
 * Implements hook_ctools_plugin_type().
 */
function entity_object_ctools_plugin_type() {
  $plugins['entity_object_handler'] = array(
    'defaults' => array(
      'type' => NULL,
    ),
  );
  return $plugins;
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function entity_object_ctools_plugin_directory($module, $plugin) {
  if ($module == 'entity_object') {
    return 'plugins/' . $plugin;
  }
  return NULL;
}

/**
 * Returns all defined Entity API handlers.
 *
 * @param string $type
 *   Filter returned handlers by type (e.g. 'field').
 *
 * @return array
 *   Array of handlers keyed by handler machine name.
 */
function entity_object_handlers($type = NULL) {

  ctools_include('plugins');
  $plugins = ctools_get_plugins('entity_object', 'entity_object_handler');

  foreach ($plugins as $key => $plugin) {
    if (empty($type) || empty($handler['type']) || $type == $plugin['type']) {
      $plugin += array(
        'label' => $key,
        'handle_list' => FALSE,
      );
      $handlers[$key] = $plugin;
    }
  }

  return $handlers;
}

/**
 * Return an Entity Object handler by its machine name.
 *
 * @param string $handler_name
 *   the requested handler machine name
 *
 * @return array|bool
 *   the requested handler as an array or FALSE if no handler if found by that
 *   name.
 */
function entity_object_handler($handler_name) {

  ctools_include('plugins');
  $plugins = ctools_get_plugins('entity_object', 'entity_object_handler');

  if (!empty($plugins[$handler_name])) {
    return $plugins[$handler_name] + array(
      'label' => $handler_name,
      'handle_list' => FALSE,
    );
  }

  return FALSE;

}

/**
 * Return the configured Entity Object API handler for a given field.
 *
 * @param string $field_name
 *   The name of the field we want to retrieve the associated handler.
 *
 * @return array|bool
 *   the requested handler as an array or FALSE if no handler if found for
 *   that field.
 */
function entity_object_field_handler($field_name) {

  $info = field_info_field($field_name);

  $handlers = entity_object_handlers('field');

  if (!empty($info['settings']['entity_object_handler']) &&
      !empty($handlers[$info['settings']['entity_object_handler']])
  ) {
    return $handlers[$info['settings']['entity_object_handler']];
  }

  return FALSE;

}
/**
 * Return the configured Entity Object API alias for a given field.
 *
 * @param string $field_name
 *   The name of the field we want to retrieve the associated alias.
 *
 * @return string|bool
 *   the requested alias or FALSE if no alias if defined for that field.
 */
function entity_object_field_alias($field_name) {

  $info = field_info_field($field_name);

  if (!empty($info['settings']['entity_object_alias'])) {
    return $info['settings']['entity_object_alias'];
  }

  return FALSE;

}

/**
 * Implements hook_form_FIELD_UI_FIELD_EDIT_FORM_alter().
 */
function entity_object_form_field_ui_field_edit_form_alter(&$form, &$form_state) {

  $handlers = entity_object_handlers('field');

  $handler_options['_none'] = t('None');

  foreach ($handlers as $key => $handler) {
    if (empty($handler['field_types']) || in_array($form['#field']['type'], $handler['field_types'])) {
      $handler_options[$key] = $handler['label'];
    }
  }

  $form['field']['settings']['entity_object_handler'] = array(
    '#type' => 'select',
    '#title' => t('Entity Object Handler'),
    '#options' => $handler_options,
    '#default_value' => !empty($form['#field']['settings']['entity_object_handler']) ? $form['#field']['settings']['entity_object_handler'] : NULL,
  );

  $form['field']['settings']['entity_object_alias'] = array(
    '#type' => 'textfield',
    '#title' => t('Entity Object Alias'),
    '#default_value' => !empty($form['#field']['settings']['entity_object_alias']) ? $form['#field']['settings']['entity_object_alias'] : NULL,
  );

}

/**
 * Menu callback to handle file uploads.
 */
function entity_object_file_upload() {

  $destination = 'private://entity_object_uploads';
  file_prepare_directory($destination, FILE_MODIFY_PERMISSIONS | FILE_CREATE_DIRECTORY);
  $file = file_save_upload('entity_object_file', array('file_validate_extensions' => array()), $destination);
  drupal_add_http_header('Content-Type', 'application/json');

  if (empty($file)) {
    print json_decode(array('error' => 'file'));
  }

  print json_encode(array('fid' => $file->fid));

  exit;

}

/**
 * Menu callback to retrieve upload progression.
 */
function entity_object_file_progress($key) {
  // TODO add some sort of security check.
  $status = uploadprogress_get_info($key);
  print json_encode($status);
}

/**
 * Access callback for entity_object_file_upload().
 */
function entity_object_file_upload_access() {
  // TODO add some sort of security check.
  return TRUE;
}
