<?php

/**
 * @file
 * Classes & interfaces definitions.
 */

/**
 * Defines Entity Object handlers execution context.
 */
interface EntityObjectContextInterface {

  /**
   * Tells handler if export is in 'tree-mode'.
   *
   * @return bool
   *   TRUE if in tree-mode, FALSE otherwise.
   */
  public function isTreeMode();

  /**
   * Returns an array of callbacks to call before every handler calls.
   *
   * @return array
   *   array of callback names.
   */
  public function handlerPreCallbacks();

  /**
   * Returns an array of callbacks to call after every handler calls.
   *
   * @return array
   *   array of callback names.
   */
  public function handlerPostCallbacks();

}


class DefaultEntityObjectContext implements EntityObjectContextInterface {

  protected $handlerPreCallbackArray = array();

  protected $handlerPostCallbackArray = array();

  protected $treeMode = FALSE;

  /**
   * Tells handler if export is in 'tree-mode'.
   *
   * @return bool
   *   TRUE if in tree-mode, FALSE otherwise.
   */
  public function isTreeMode() {
    return $this->treeMode;
  }

  /**
   * Set whether this context is in tree-mode or not.
   *
   * @param bool $tree_mode
   *   TRUE if context is in tree-mode, FALSE otherwise.
   */
  public function setTreeMode($tree_mode) {
    $this->treeMode = $tree_mode;
  }

  /**
   * Returns an array of callbacks to call before every handler calls.
   *
   * @return array
   *   array of callback names.
   */
  public function handlerPreCallbacks() {
    return $this->handlerPreCallbackArray;
  }

  /**
   * Register a new handler pre-execution callback.
   *
   * @param string $name
   *   the callback function name
   */
  public function registerPreCallback($name) {
    if (!in_array($name, $this->handlerPreCallbackArray)) {
      $this->handlerPreCallbackArray[] = $name;
    }
  }

  /**
   * Returns an array of callbacks to call after every handler calls.
   *
   * @return array
   *   array of callback names.
   */
  public function handlerPostCallbacks() {
    return $this->handlerPostCallbackArray;
  }

  /**
   * Register a new handler post-execution callback.
   *
   * @param string $name
   *   the callback function name
   */
  public function registerPostCallback($name) {
    if (!in_array($name, $this->handlerPostCallbackArray)) {
      $this->handlerPostCallbackArray[] = $name;
    }
  }
}
