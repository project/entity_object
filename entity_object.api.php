<?php

/**
 * @file
 * Documentation of hooks defined by entity_object module
 */

/**
 * Describe entity types allowed to be exported in tree-mode.
 *
 * This is used by the entityreference handler to determine which entity types
 * are exported as full objects or id in tree-mode.
 *
 * @return array
 *   an associative array of associative arrays keyed by entity type whose sub
 *   keys are :
 *     - bundles: an array of bundle names that support tree-mode export. if
 *       omitted, all defined bundles of this entity type supports tree-mode
 *       export.
 */
function hook_entity_object_tree_types() {
  $entity_types['node'] = array(
    'bundles' => array('page', 'article'),
  );
}
