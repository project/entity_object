<?php

/**
 * @file
 * Entity Object API handler using entity_metadata_wrapper API
 */

$plugin = array(
  'label' => 'Entity Metadata Wrapper',
  'entity_object_export' => 'entity_object_handler_entity_metadata_wrapper_export',
  'entity_object_import' => 'entity_object_handler_entity_metadata_wrapper_import',
);

/**
 * Export a wrapper property value.
 *
 * @param EntityMetadataWrapper $wrapper
 *   the wrapped value
 * @param EntityObjectContextInterface $context
 *   TRUE if in tree-mode, FALSE otherwise.
 *
 * @return array|object
 *   The resulting export.
 */
function entity_object_handler_entity_metadata_wrapper_export($wrapper, $context) {
  return $wrapper->value();
}

/**
 * Import a wrapper property value.
 *
 * @param EntityMetadataWrapper $wrapper
 *   the target wrapper
 * @param array|object $value
 *   the value to import
 * @param EntityObjectContextInterface $context
 *   TRUE if in tree-mode, FALSE otherwise.
 *
 * @return array|object
 *   The field value.
 */
function entity_object_handler_entity_metadata_wrapper_import($wrapper, $value, $context) {
  if (!empty($wrapper)) {
    $info = $wrapper->info();
    if (!empty($info['setter callback'])) {
      $wrapper->set($value);
    }
  }
}
