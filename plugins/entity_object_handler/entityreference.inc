<?php

/**
 * @file
 * Entity Object API handler for entityreference fields
 */

$plugin = array(
  'label' => 'Entity Reference',
  'type' => 'field',
  'handle_list' => TRUE,
  'field_types' => array('entityreference'),
  'entity_object_export' => 'entity_object_handler_entityreference_export',
  'entity_object_import' => 'entity_object_handler_entityreference_import',
);

/**
 * Export an entityreference field.
 *
 * @param EntityMetadataWrapper $wrapper
 *   the wrapped value
 * @param EntityObjectContextInterface $context
 *   TRUE if in tree-mode, FALSE otherwise.
 *
 * @return array|object
 *   The resulting export.
 */
function entity_object_handler_entityreference_export($wrapper, $context) {

  if (entity_property_list_extract_type($wrapper->type())) {
    $result = array();
    foreach ($wrapper as $delta => $item) {
      $item_result = entity_object_handler_entityreference_export($item, $context);
      if ($item_result !== NULL) {
        $result[] = $item_result;
      }
    }
    return $result;
  }

  $entity = entity_load_single($wrapper->type(), $wrapper->getIdentifier());
  if ($entity) {
    if (!empty($context) && $context->isTreeMode() && in_array($wrapper->type(), array_keys(entity_object_tree_entity_types()))) {
      return entity_object_export(
        $entity, $context
      );
    }

    $object_reference = (object) array(
      'entity_type' => $wrapper->type(),
      'target_id' => $wrapper->getIdentifier(),
    );

    if (module_exists('uuid')) {
      $object_reference->target_uuid = $wrapper->uuid->value();
    }

    return $object_reference;
  }
  else {
    return NULL;
  }
}

/**
 * Import an entityreference value.
 *
 * @param EntityMetadataWrapper $wrapper
 *   the target wrapper
 * @param array|object $value
 *   the value to import
 * @param EntityObjectContextInterface $context
 *   TRUE if in tree-mode, FALSE otherwise.
 *
 * @return array|object
 *   The field value.
 */
function entity_object_handler_entityreference_import($wrapper, $value, $context) {

  if (entity_property_list_extract_type($wrapper->type())) {
    foreach ($value as $delta => $item) {
      entity_object_handler_entityreference_import($wrapper->get($delta), $item, $context);
    }
    // TODO thoroughly test what's follows.
    if (count($wrapper) > count($value)) {
      array_splice($wrapper, 0, count($value));
    }
  }
  else {

    $entity_type = $wrapper->type();
    $current_target_id = $wrapper->raw();

    // We have a referenced UUID.
    if (module_exists('uuid') && !empty($value['target_uuid'])) {
      $results = entity_get_id_by_uuid($entity_type, array($value['target_uuid']));
      $target_id = reset($results);
      if (!empty($target_id) && !empty($value['target_id']) && $value['target_id'] != $target_id) {
        watchdog('entity_object', "The target_id !target_id and target_uuid !target_uuid (resolved to !resolved_target_id) don't match", array(
          '!target_id' => $value['target_id]'],
          '!target_uuid' => $value['target_uuid'],
          '!resolved_target_id' => $target_id,
        ),
        WATCHDOG_WARNING);
      }
    }
    // We have a referenced internal id.
    elseif (!empty($value['target_id'])) {
      $target_id = $value['target_id'];
    }

    // We found or resolved a target_id.
    if (!empty($target_id)) {
      // Test is there's actually something to update to save some cycles.
      if (empty($current_target_id) || $current_target_id != $target_id) {
        $entity = entity_load_single($entity_type, $target_id);
        if (!empty($entity)) {
          $entity_wrapper = entity_metadata_wrapper($entity_type, $entity);
          $wrapper->set($entity_wrapper);
        }
        else {
          watchdog('entity_object', "Cannot load referenced !type by id !id", array(
            '!type' => $entity_type,
            '!id' => $target_id,
          ), WATCHDOG_ERROR);
        }
      }
    }
    // No target_id, maybe there's actual data to import.
    else {

      // Check if the currently associated entity is to be updated or if a
      // new entity has to be created.
      $target_entity = $wrapper->value();

      if (!empty($target_entity)) {
        $entity_wrapper = entity_metadata_wrapper($entity_type, $target_entity);
      }

      // No actual data or different entity, create a new entity.
      if (empty($entity_wrapper) || !entity_object_handler_entityreference_same($entity_wrapper, $value)) {

        // Try to retrieve an existing reference by UUID, if given.
        if (module_exists('uuid') && !empty($value['uuid'])) {
          $results = entity_uuid_load($entity_type, array($value['uuid']));
          $existing_ref_entity = reset($results);
        }

        // If no entity found, create a new one.
        if (empty($existing_ref_entity)) {
          $entity_info = $wrapper->entityInfo();
          $bundle_key = $entity_info['entity keys']['bundle'];

          if (!empty($entity_info['bundles'][$value[$bundle_key]])) {
            $initial_data = array($bundle_key => $value[$bundle_key]);
            if (isset($value['uuid'])) {
              $initial_data['uuid'] = $value['uuid'];
            }
            $existing_ref_entity = entity_create($entity_type, $initial_data);

          }
        }

        if (!empty($existing_ref_entity)) {
          $target_entity_wrapper = entity_metadata_wrapper($entity_type, $existing_ref_entity);
        }

      }
      // Existing entity match incoming data, let's update.
      elseif (entity_object_handler_entityreference_same($entity_wrapper, $value)) {
        $target_entity_wrapper = $entity_wrapper;
      }

      if (!empty($target_entity_wrapper)) {
        entity_object_import((object) $value, $target_entity_wrapper->value(), $context);
        $target_entity_wrapper->save();
        $wrapper->set($target_entity_wrapper);
      }
    }
  }

}

/**
 * Test if a given entity match a Entity Object incoming data.
 *
 * This is used to determine if inline content of entityreference fields match
 * existing value or if the creation of a new entity is needed.
 *
 * @param EntityMetadataWrapper $entity_wrapper
 *   the wrapper representing the existing referenced entity
 * @param array $data
 *   the incoming Object API data (TODO should be an object?)
 *
 * @return bool
 *   TRUE if the data should be used to update the existing entity or FALSE if
 *   a new entity should be created.
 */
function entity_object_handler_entityreference_same($entity_wrapper, $data) {

  $entity_info = $entity_wrapper->entityInfo();
  $bundle_key = $entity_info['entity keys']['bundle'];

  // UUIDs exist and match.
  if (!empty($entity_wrapper->uuid) && !empty($data['uuid']) && $entity_wrapper->uuid->value() == $data['uuid']) {
    return TRUE;
  }

  // TODO match on internal ID if uuid are unavailable.
  // TODO what about internal ID match but not uuid ?
  // Data doesn't provide UUID, check bundle.
  if (!isset($data['uuid']) && $entity_wrapper->getBundle() == $data[$bundle_key]) {
    return TRUE;
  }

  return FALSE;

}
