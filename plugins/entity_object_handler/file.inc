<?php

/**
 * @file
 * Entity Object API handler for entityreference fields
 */

$plugin = array(
  'label' => 'File',
  'type' => 'field',
  'field_types' => array('file', 'image'),
  'entity_object_export' => 'entity_object_handler_file_export',
  'entity_object_import' => 'entity_object_handler_file_import',
);

/**
 * Export a file (or image) field.
 *
 * @param EntityMetadataWrapper $wrapper
 *   the wrapped value
 * @param EntityObjectContextInterface $context
 *   TRUE if in tree-mode, FALSE otherwise.
 *
 * @return array|object
 *   The resulting export.
 */
function entity_object_handler_file_export($wrapper, $context) {
  $file = $wrapper->value();
  return (object) array(
    'id' => $file['fid'],
    'type' => $file['type'],
    'url' => file_create_url($file['uri']),
    'mime' => $file['filemime'],
  );
}


/**
 * Import a file value.
 *
 * @param EntityMetadataWrapper $wrapper
 *   the target wrapper
 * @param array|object $value
 *   the value to import
 * @param EntityObjectContextInterface $context
 *   Import context.
 *
 * @return array|object
 *   The field value.
 */
function entity_object_handler_file_import($wrapper, $value, $context) {
  if (!empty($wrapper)) {
    $value = (object) $value;
    $file = file_load($value->id);
    if ($file) {
      if (!$file->status) {
        $file->status = FILE_STATUS_PERMANENT;
        file_save($file);
      }
      $wrapper->set(array('fid' => $file->fid, 'display' => 1));
    }
  }
}
