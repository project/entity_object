<?php

/**
 * @file
 * Entity Object API handler for entityreference fields
 */

$plugin = array(
  'label' => 'Properties',
  'type' => 'field',
  'field_types' => array('field_properties'),
  'handle_list' => TRUE,
  'entity_object_export' => 'entity_object_handler_field_properties_export',
  'entity_object_import' => 'entity_object_handler_field_properties_import',
);

/**
 * Export a wrapper property value.
 *
 * @param EntityMetadataWrapper $wrapper
 *   the wrapped value
 * @param EntityObjectContextInterface $context
 *   TRUE if in tree-mode, FALSE otherwise.
 *
 * @return array|object
 *   The resulting export.
 */
function entity_object_handler_field_properties_export($wrapper, $context) {
  $value = array();
  if (entity_property_list_extract_type($wrapper->type())) {
    foreach ($wrapper as $item) {
      switch ($item->type->value()) {
        case 'number':
          $value[$item->name->value()] = floatval($item->value->value());
          break;

        case 'json':
          $value[$item->name->value()] = json_decode($item->value->value());
          break;

        default:
          $value[$item->name->value()] = $item->value->value();
          break;
      }
    }
  }
  return (object) $value;
}

/**
 * Import a wrapper property value.
 *
 * @param EntityMetadataWrapper $wrapper
 *   the target wrapper
 * @param array|object $value
 *   the value to import
 * @param EntityObjectContextInterface $context
 *   TRUE if in tree-mode, FALSE otherwise.
 *
 * @return array|object
 *   The field value.
 */
function entity_object_handler_field_properties_import($wrapper, $value, $context) {
  if (entity_property_list_extract_type($wrapper->type())) {
    foreach ($wrapper as $delta => $item) {
      $deltas[$item->name->value()] = $delta;
    }
  }

  foreach ($value as $key => $item_value) {

    if (is_object($item_value) || is_array($item_value) || is_bool($item_value)) {
      $item_value = json_encode($item_value);
      $item_type = 'json';
    }
    elseif (is_numeric($item_value)) {
      $item_type = 'number';
    }
    else {
      $item_type = 'string';
    }

    if (isset($deltas[$key])) {
      $wrapper[$deltas[$key]]->type->set($item_type);
      $wrapper[$deltas[$key]]->value->set($item_value);
    }
    else {
      $wrapper[] = array(
        'name' => $key,
        'type' => $item_type,
        'value' => $item_value,
      );
    }

  }

}
