<?php

/**
 * @file
 * Entity Object API handler for language_field fields.
 */

$plugin = array(
  'label' => 'Field language',
  'type' => 'field',
  'field_types' => array('language_field'),
  'entity_object_export' => 'entity_object_handler_field_language_export',
  'entity_object_import' => 'entity_object_handler_field_language_import',
);

/**
 * Export a language_field value.
 *
 * @param EntityMetadataWrapper $wrapper
 *   the wrapped value
 * @param EntityObjectContextInterface $context
 *   TRUE if in tree-mode, FALSE otherwise.
 *
 * @return array|object
 *   The resulting export.
 */
function entity_object_handler_field_language_export($wrapper, $context) {
  return $wrapper->value()["value"];
}

/**
 * Import a language_field value.
 *
 * @param EntityMetadataWrapper $wrapper
 *   the target wrapper
 * @param array|object $value
 *   the value to import
 * @param EntityObjectContextInterface $context
 *   TRUE if in tree-mode, FALSE otherwise.
 *
 * @return array|object
 *   The field value.
 */
function entity_object_handler_field_language_import($wrapper, $value, $context) {
  if (!empty($wrapper)) {
    $info = $wrapper->info();
    if (!empty($info['setter callback'])) {
      $wrapper->set(array('value' => $value));
    }
  }
}
